'use strict'

const Database = use('Database')
const { validate } = use('Validator')

class UserController {
    async addTrash({ request, response }) {
        const { nama_pembuang, tanggal, status, lokasi_sampah, berat_sampah } = request.all()

        const rules = {
            nama_pembuang: 'required',
            tanggal: 'required',
            status: 'required',
            lokasi_sampah: 'required',
            berat_sampah: 'required'
        }

        const uniqueNum = new Date().getTime()
        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }
        //get file voice
        const foto = request.file('foto_sampah', {
            extnames: ['jpg', 'jpeg', 'png']
        })

        let image_route = ''

        if (foto) {
            if (foto.size > 10000000) {
                response.status(400)
                return {
                    message: "Maximum upload file size is 10MB",
                    field: "foto_sampah",
                    validation: "required"
                }

            }
            await foto.move(`./public/upload/`, { //syntax meminndahkan
                name: `foto_sampah_${uniqueNum}.jpg`,
                overwrite: true
            })
            image_route = `upload/foto_sampah_${uniqueNum}.jpg`
        }

        await Database.raw(`insert into m_trash (nama_pembuang, tanggal, status, lokasi_sampah, foto_sampah, berat_sampah, created_at, updated_at) values ('${nama_pembuang}', '${tanggal}', '${status}', '${lokasi_sampah}', '${image_route}', '${berat_sampah}',now()::timestamp,now()::timestamp)`)
        const result = await Database.raw(`select * from m_trash`)
        return result.rows
    }

    async labelView({ request, response }) {
        const { user } = request.all()
        const rules = {
            user: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }

        try {
            const penambahan = await Database.raw(`Select count(id) as count from m_trash where nama_pembuang='${user}'`)
            const history = await Database.raw(`Select count(id) as count from m_trash where status='selesai' and nama_pembuang='${user}'`)
            const result = {
                riwayat: penambahan.rows,
                selesai: history.rows
            }
            return result
        } catch (error) {
            return error
        }
    }

    async listTrash({ request, response }) {
        const { user } = request.all()
        const rules = {
            user: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }

        try {
            const result = await Database.raw(`select * from m_trash where nama_pembuang='${user}'`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async detailTask({ request, response }) {
        const { id } = request.all()
        const rules = {
            id: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }
        try {
            const result = await Database.raw(`select * from m_trash where id='${id}'`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async updateTask({ request, response }) {
        const { id, nama_pembuang, tanggal, status, lokasi_sampah, berat_sampah } = request.all()
        const rules = {
            id: 'required',
            nama_pembuang: 'required',
            tanggal: 'required',
            status: 'required',
            lokasi_sampah: 'required',
            berat_sampah: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }
        try {
            await Database.raw(`update m_trash set nama_pembuang='${nama_pembuang}', tanggal='${tanggal}', status='${status}', lokasi_sampah='${lokasi_sampah}', berat_sampah='${berat_sampah}', updated_at=now()::timestamp where id='${id}'`)
            const result = await Database.raw(`select * from m_trash where id='${id}'`)
            return result.rows
        } catch (error) {
            return error
        }
    }
}

module.exports = UserController
