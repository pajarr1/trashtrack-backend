'use strict'

const Database = use('Database')
const { validate } = use('Validator')

class WorkerController {

    async listWorker({ }) {
        try {
            const result = await Database.raw(`select * from m_users where role='petugas'`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async checkCeklis({ request }) {
        try {
            const { id, username } = request.all()

            // const result = await Database.raw(`select m_users where id='${id}'`)

            const result = await Database.raw(`select * from m_task where pekerja='${username}`)


            return result.rows

        } catch (error) {
            return error;
        }
    }

    async chartTrash({ request, response }) {
        try {
            const belum = await Database.raw(`Select count(id) as count, tanggal as date from m_trash where status='tunggu' group by tanggal`)
            const semua = await Database.raw(`Select count(id) as count, tanggal as date from m_trash group by tanggal`)
            const result = {
                selesaidiangkut: belum.rows,
                semuasampah: semua.rows
            }
            return result
        } catch (error) {
            return error
        }
    }

    async chartTrashTersisa({ request, response }) {
        try {
            const result = await Database.raw(`Select count(id) as count, tanggal as date from m_trash where status='Belum' group by tanggal`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async workerTask({ request, response }) {
        try {
            const result = await Database.raw(`select * from m_task`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async history({ request, response }) {
        try {
            const result = await Database.raw(`select * from m_trash where status='selesai'`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async finishingTask({ request, response }) {
        const { id, berat_sampah, tanggal_selesai } = request.all()
        const rules = {
            id: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }
        try {
            await Database.raw(`update m_task set berat_sampah='${berat_sampah}', tanggal_selesai='${tanggal_selesai}' where id='${id}'`)
            const result = await Database.raw(`select * from m_task`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async updateStatus({ request, response }) {
        const { id } = request.all()
        const rules = {
            id: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }

        try {
            await Database.raw(`update m_trash set status='proses' where id='${id}'`)
            const result = await Database.raw(`select * from m_task`)
            return result.rows
        } catch (error) {
            return error
        }
    }
}

module.exports = WorkerController
