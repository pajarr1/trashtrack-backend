'use strict'

const Database = use('Database')
const { validate } = use('Validator')
const Env = use('Env')
const Hash = use('Hash')
const jwt = require('jsonwebtoken')
const { hashSync: hash, compareSync: compare } = require('bcryptjs')

class AdminController {
    async logout({ auth, request, response }) {
        let { email } = request.all()
        try {
            await Database.raw(`UPDATE m_users SET is_login = false WHERE email = '${email}';`)
            return "Logout success!"
        } catch (error) {
            response.status(500)
        }
    }

    async register({ request, response, params }) {

        let { username, email, password, password2, no_hp } = request.all()

        email = email.toLowerCase()

        const rules = {
            username: 'required|unique:public.m_users,username',
            email: 'required|unique:public.m_users,email',
            password: 'required',
            password2: 'required'
        }

        if (password2 != password) {
            response.status(409)
            return {
                message: `Not Match`
            }
        }

        const pass = await Hash.make(password)

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }

        await Database.raw(`insert into m_users(username, email, password, no_hp, created_at, updated_at) values ('${username}','${email}','${pass}', '${no_hp}',now()::timestamp,now()::timestamp)`)


        try {
            return {
                message: 'Register Success',
                username: username,
                email: email,
                no: no_hp,
            }
        } catch (error) {
            console.log(error);
            if (error.code == '23505') {
                response.status(409)
                return {
                    message: `Email '${email}' already exist`
                }
            } else {
                response.status(500)
            }
        }
    }

    async checkrole({ request }) {
        const { user_id, username } = request.all()
        const response = await Database.raw(`select * from m_users where id = ${user_id} AND username = '${username}'`)

        return response.rows[0]
    }

    async login({ auth, request, params, response }) {
        const { email, password } = request.all()

        const rules = {
            email: 'required',
            password: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }

        let userLogin = email
        userLogin = userLogin.toLowerCase() // conver ke hurup kecil

        const checkuser = await Database.raw(`select * from m_users where email = '${email}'`)
        if (checkuser.rows.length === 0) {
            response.status(400)
            return {
                success: false,
                message: 'email is does not exist'
            }
        }

        if (compare(password, checkuser.rows[0].password)) {
            let dataUser = checkuser.rows[0]

            let user = {
                username: dataUser.username,
                email: dataUser.email,
                role: dataUser.role,
                is_login: dataUser.is_login,
                id: dataUser.id
            }
            const token = jwt.sign(user, Env.get('APP_KEY'));
            const result = {
                user: user,
                token: token
            }

            await Database.raw(`UPDATE m_users SET is_login = true WHERE email = '${email}';`)

            return result
        } else {
            response.status(401)
            return {
                message: 'Email atau Password salah!'
            }
        }
    }

    async listUser({ request, response }) {
        const result = await Database.raw(`select * from m_users`)
        try {
            return result.rows
        } catch (error) {
            return error
        }
    }

    async deleteUser({ request, response }) {
        const { id } = request.all()
        const rules = {
            id: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }

        try {
            await Database.raw(`delete from m_users where id='${id}'`)
            const result = await Database.raw(`select * from m_users`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async updateUser({ request, response }) {
        let { id, username, role, email, password, no_hp } = request.all()
        email = email.toLowerCase()

        const rules = {
            id: 'required',
            username: 'required',
            role: 'required',
            no_hp: 'required',
            email: 'required',
            password: 'required'
        }
        const pass = await Hash.make(password)

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }
        try {
            await Database.raw(`update m_users set username='${username}', role='${role}', no_hp='${no_hp}', email='${email}', password='${pass}', updated_at=now()::timestamp where id=${id}`)

            const result = await Database.raw(`select * from m_users`)
            return result.rows
        } catch (error) {
            console.log(error);
            if (error.code == '23505') {
                response.status(409)
                return {
                    message: `Email '${email}' already exist`
                }
            } else {
                response.status(500)
            }
        }
    }

    async addUser({ request, response, params }) {
        let { username, role, email, password, no_hp } = request.all()

        const rules = {
            username: 'required|unique:public.m_users,username',
            role: 'required',
            email: 'required|unique:public.m_users,email',
            password: 'required',
        }

        const pass = await Hash.make(password)

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }

        await Database.raw(`insert into m_users( username, role, email, password, no_hp, created_at, updated_at) values ('${username}','${role}','${email}','${pass}', '${no_hp}',now()::timestamp,now()::timestamp)`)


        try {
            const result = await Database.raw(`select * from m_users`)
            return result.rows
        } catch (error) {
            console.log(error);
            if (error.code == '23505') {
                response.status(409)
                return {
                    message: `Email '${email}' already exist`
                }
            } else {
                response.status(500)
            }
        }
    }

    async showTrash({ request, response }) {
        try {
            const result = await Database.raw(`select * from m_trash`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async dataTask({ request, response }) {
        const { id } = request.all()
        const rules = {
            id: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }

        try {
            const pekerja = await Database.raw(`select * from m_users where role='Pekerja'`)
            const pemilik = await Database.raw(`select * from m_trash where id=${id}`)
            const result = {
                pemilik: pemilik.rows[0].nama_pembuang,
                pekerja: pekerja.rows
            }
            return result
        } catch (error) {
            return error
        }
    }

    async insertTask({ request, response }) {
        const { pemilik_sampah, pekerja, tanggal_mulai, tanggal_selesai } = request.all()
        const rules = {
            pemilik_sampah: 'required',
            pekerja: 'required',
            tanggal_mulai: 'required',
            tanggal_selesai: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {
            response.status(400)
            return validation.messages()
        }
        try {
            await Database.raw(`insert into m_task (pemilik_sampah, pekerja, tanggal_mulai, tanggal_selesai, created_at, updated_at) values ('${pemilik_sampah}','${pekerja}','${tanggal_mulai}','${tanggal_selesai}',now()::timestamp,now()::timestamp)`)
            return {
                success: true,
                message: 'Success'
            }
        } catch (error) {
            return error
        }
    }

    //linechart
    async trashRemaining({ request, response }) {
        try {
            const result = await Database.raw(`Select count(id) as count, tanggal as date from m_trash group by status='tunggu',tanggal`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async jumlahPekerja({ request, response }) {
        try {
            const result = await Database.raw(`select count(id) as role from m_users where role='Pekerja'`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async beratSampah({ request, response }) {
        try {
            const result = await Database.raw(`SELECT sum(berat_sampah) as berat FROM m_trash WHERE created_at >= DATE(NOW()) - INTERVAL '7' DAY`)
            return result.rows
        } catch (error) {
            return error
        }
    }

    async sampahOrganik({ request, response }) {
        try {
            const result = await Database.raw(`select count (id) as total from m_trash where jenis_sampah='organik'`)
            return result.rows
        }
        catch (error) {
            return error
        }
    }

    async sampahAnorganik({ request, response }) {
        try {
            const result = await Database.raw(`select count (id) as total from m_trash where jenis_sampah='anorganik'`)
            return result.rows
        }
        catch (error) {
            return error
        }
    }

    async sampahRumahTangga({ request, response }) {
        try {
            const result = await Database.raw(`select count (id) as total from m_trash where jenis_sampah='rumah tangga'`)
            return result.rows
        }
        catch (error) {
            return error
        }
    }
}

module.exports = AdminController

