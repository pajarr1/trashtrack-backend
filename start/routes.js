'use strict'

const { RouteGroup, route, RouteResource } = require('@adonisjs/framework/src/Route/Manager')


/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

const api = 'api/v1'
const exts = ['json', 'table', 'tablejson', 'jsontable']

Route.on('/').render('welcome')

Route.group(() => {
    Route.post('/login', 'AdminController.login')
    Route.post('/logout', 'AdminController.logout')
    Route.post('/register', 'AdminController.register')
    Route.get('/list_users', 'AdminController.listUser')
    Route.post('/delete_users', 'AdminController.deleteUser')
    Route.post('/update_users', 'AdminController.updateUser')
    Route.post('/add_user', 'AdminController.addUser')
    Route.get('/show_trash', 'AdminController.showTrash')
    Route.get('/data_task', 'AdminController.dataTask')
    Route.get('/linechart_remaining', 'AdminController.trashRemaining')
    Route.post('/add_task', 'AdminController.insertTask')
    Route.get('/worker_task', 'WorkerController.workerTask')
    Route.get('/check_ceklis', 'WorkerController.checkCeklis')
    Route.get('/checkrole', 'AdminController.checkrole')
    Route.post('/finishing_task', 'WorkerController.finishingTask')
    Route.get('/history', 'WorkerController.history')
    Route.get('/chart_trash', 'WorkerController.chartTrash')
    Route.get('/chart_tersisa', 'WorkerController.chartTrashTersisa')

    Route.post('/add_trash', 'UserController.addTrash')
    Route.post('/label_view', 'UserController.labelView')
    Route.post('/list_trash', 'UserController.listTrash')
    Route.post('/update_status', 'WorkerController.updateStatus')
    Route.get('/detail_task', 'UserController.detailTask')
    Route.post('/update_task', 'UserController.updateTask')

    Route.get('/list_worker', 'WorkerController.listWorker')
    Route.get('/jumlah_pekerja', 'AdminController.jumlahPekerja')
    Route.get('/berat_sampah', 'AdminController.beratSampah')
    Route.get('/sampah_organik', 'AdminController.sampahOrganik')
    Route.get('/sampah_anorganik', 'AdminController.sampahAnorganik')
    Route.get('/sampah_rumahtangga', 'AdminController.sampahRumahTangga')
}).prefix(api).formats(exts)